﻿using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using MSU_DotNet_OAuth.Filters;
using MSU_DotNet_OAuth.Models;
using MSU_DotNet_OAuth.OAuthHelpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MSU_DotNet_OAuth.Controllers
{
    [InitializeSimpleMembership]
    public class OAuthController : Controller
    {
        //
        // GET: /OAuth/
        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            
            /* This can replace the line below if the state gets passed back like 
             * with other OAuth providers that I've used.
             */
            //MsuOAuth2Client.RewriteRequest();

            /* This needs to be here again, as with the state not being carried over,
             * the OAuth framework does not know the provider
             */
            var msuClient = new MsuOAuth2Client(ConfigurationManager.AppSettings["MsuClientId"],
                ConfigurationManager.AppSettings["MsuClientSecret"],
                ConfigurationManager.AppSettings["MsuRedirectUri"]);
            /* This overrides the Microsoft OAuth */
            AuthenticationResult result = ((MsuOAuth2Client)msuClient).VerifyAuthentication(HttpContext, new Uri(ConfigurationManager.AppSettings["MsuRedirectUri"]));

            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            {
                return RedirectToLocal(returnUrl);
            }

            if (User.Identity.IsAuthenticated)
            {
                // If the current user is logged in add the new account
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
