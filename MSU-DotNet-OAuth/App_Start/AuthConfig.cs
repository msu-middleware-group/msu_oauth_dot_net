﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using MSU_DotNet_OAuth.Models;
using MSU_DotNet_OAuth.OAuthHelpers;
using System.Configuration;

namespace MSU_DotNet_OAuth
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");

            //OAuthWebSecurity.RegisterFacebookClient(
            //    appId: "",
            //    appSecret: "");

            //OAuthWebSecurity.RegisterGoogleClient();
            
            /* Uncomment this out to use MSU Auth
            var msuExtra = new Dictionary<string, object>();
            msuExtra.Add("Icon", "~/Content/msunet.png");
            var msuClient = new MsuOAuth2Client(ConfigurationManager.AppSettings["MsuClientId"],
                ConfigurationManager.AppSettings["MsuClientSecret"],
                ConfigurationManager.AppSettings["MsuRedirectUri"], msuExtra);

            OAuthWebSecurity.RegisterClient(msuClient, "MSU", msuExtra);
             */
        }
    }
}
